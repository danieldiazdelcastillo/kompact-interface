# Kompact Interface
Interface for adding articles to the Kompact database. Created for unix based operating systems.

## Dependencies for Stanford CoreNLP
Some dependencies are included in this repo while others must be installed
- Bootstrap 3 (included)
- pycurl
- Ant
- Simpleframework (included)
- Stanford CoreNLP v3.3.1 (included)

## Languages used for summarization
- Java
- Python
- PHP

## Installation
Clone the repositories into a directory where you want to keeo them
```sh
$ mkdir myDir
$ cd myDir
$ git clone http://kompact-interface-url.git # the interface
$ git clone http://kompact-server-url.git # the server
```

## Usage
Make sure the Kompact server is running before you open the interface. If you start the interface before starting the server, you will reeive a notification in the top-lft corner of the screen telling you the server has not started. If the server is not running, you will not be able to generate any summaries.

### Starting the server
1) To start the server, go to the directory containing the server
```sh
$ cd path/to/myDir/StanfordCoreNLPDir
```

2) If you have not already, install the proper jar files into the `lib` directory. You do not need to do this step if the lib folder already exists
```sh
$ ant libs
```

3) To run the server on `localhost`
```sh
$ ant run
```
This will actually run 2 servers, one in Java that generates the XML file that will be sent to the Python server which generates the actual JSON summary to be used.

### Opening the interface
1) Go to the directory containing the interface.
```sh
$ cd path/to/myDir/kompactInterfaceDirectory
```

2) Run a PHP server on `localhost:9999` to enable PHP post requests to the database and the already running servers
```sh
$ php -S localhost:9999
```

3) The server is now running at `localhost:9999`. On a browser, you can enter `localhost:9999` to use the interface.

### Closing the interface and servers
To close either the interfae or servers, just `Ctrl+C`.
