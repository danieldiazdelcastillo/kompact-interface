<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

$all_summaries = $_POST["as"];
$summary = $_POST["s"];
$category = $_POST["c"];
$link = $_POST["l"];
$title = $_POST["t"];
$authors = $_POST["a"];
$pubDate = $_POST["p"];
$imageLink = $_POST["i"];
$source = $_POST["source"];
$ot = $_POST["ot"];

$content = array(
	"as" => $all_summaries,
	"s" => $summary,
	"c" => $category,
	"l" => $link,
	"t" => $title,
	"a" => $authors,
	"p" => $pubDate,
	"i" => $imageLink,
	"source" => $source,
	"ot" => $ot
);

if (isset($_POST["id"]))
	$content["id"] = $_POST["id"];

$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded; charset=UTF-8",
        'method'  => 'POST',
        'timeout' => 300,
        'content' => http_build_query($content)
    )
);
$context  = stream_context_create($options);

$response = file_get_contents("http://kompactit.com/php/push-to-database.php", false, $context);

echo $response;

?>