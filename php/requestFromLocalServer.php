<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

require_once 'medoo.min.php';

/*
Can retrieve articles:
1. individually through IDs
2. Latest of all articles
3. Latest of specified articles
4. Popular of all articles
5. Popular of specified articles

Parameters:

*/

// Latest by default; accepts JSON encoded array of categories
// category is either "latest", "popular", or JSON string of categories
$category = "latest";
$start = 0; // 0 by default
$count = 10; // # of articles to get
$id = ""; // just get the article from the id


if (isset($_GET["category"]))
    $category = $_GET["category"];
if (isset($_GET["start"]))
    $start = $_GET["start"];
if (isset($_GET["count"]))
    $count = $_GET["count"];
if (isset($_GET["id"]))
    $id = $_GET["id"];


$database = new medoo([
    'database_type' => 'mysql',
    'database_name' => 'kompact_articles',
    'server' => 'localhost',
    'charset' => 'utf8',
    'username' => 'root',
    'password' => ''
]);

// Check for connection errors
if ($database->error()[1]){
    echo json_encode($database->error()) . "\n";
    return;
}


$articles = array();
// 1 article with ID
if ($id !== ""){
    $articles = $database->select("articles6","*",array(
        "Article_ID" => $id
    ));
}
// All articles by publication date
else if ($category === "latest"){
    $articles = $database->select("articles6","*",array(
        "ORDER" => "Pub_Date DESC",
        "LIMIT" => [$start,$count]
    ));
}
// All articles by popularity
else if ($category === "popular"){
    $articles = $database->select("articles6","*",array(
        "ORDER" => "Score DESC",
        "LIMIT" => [$start,$count]
    ));
}
else {
    // Category is JSON encoded
    $category = json_decode($category);
    $articles = $database->select("articles6","*",array(
        "Category" => $category,
        "ORDER" => "Pub_Date DESC",
        "LIMIT" => [$start,$count]
    ));
}


echo json_encode($articles);

?>
