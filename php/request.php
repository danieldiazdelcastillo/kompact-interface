<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

$raw_text = $_POST["text"];
$serverURL = $_POST["url"];

$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded; charset=UTF-8",
        'method'  => 'POST',
        'timeout' => 300,
        'content' => http_build_query(array("text" => $raw_text))
    )
);
$context  = stream_context_create($options);

// The first arg is the url to send the request to
// http://localhost:8081
$response = file_get_contents($serverURL, false, $context);

echo $response;

?>