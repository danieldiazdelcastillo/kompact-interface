<?php

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

require_once 'medoo.min.php';

$all_summaries = $_POST["as"];
$summary = $_POST["s"];
$category = $_POST["c"];
$link = $_POST["l"];
$title = $_POST["t"];
$authors = $_POST["a"];
$pubDate = $_POST["p"];
$imageLink = $_POST["i"];
$source = $_POST["source"];
$ot = $_POST["ot"];

$id = "";
if (isset($_POST["id"]))
  $id = $_POST["id"];

// Establish a connection using medoo with options depending on accessing localhost or GAE
$database = null;
if (isset($_SERVER['SERVER_SOFTWARE']) && strpos($_SERVER['SERVER_SOFTWARE'],'Google App Engine') !== false){
    $database = new medoo([
        'database_type' => 'mysql',
        'database_name' => 'kompact_articles',
        'socket' => '/cloudsql/omega-granite-800:kompact',
        'charset' => 'utf8',
        'username' => 'root',
        'password' => ''
    ]);
}
// On dev server
else {
    $database = new medoo([
        'database_type' => 'mysql',
        'database_name' => 'kompact_articles',
        'server' => 'localhost',
        'charset' => 'utf8',
        'username' => 'root',
        'password' => ''
    ]);
}

// Check for connection errors
if ($database->error()[1]){
    echo json_encode($database->error()) . "\n";
    return;
}

if ($id !== ""){
    $database->update("articles6",array(
        "Category" => $category,
        "Link" => $link,
        "Title" => $title,
        "#Date_Added" => "CURRENT_TIMESTAMP()",
        "Author" => $authors,
        "Image_Link" => $imageLink,
        "Summary" => $summary,
        "#Pub_Date" => "STR_TO_DATE('$pubDate', '%m/%d/%Y %l:%i %p')",
        "Source" => $source,
        "All_Summaries" => $all_summaries,
        "Original_Text" => $ot
    ),array(
        "Article_ID" => $id
    ));
}
else {
    $database->insert("articles6",array(
        "#Article_ID" => "UUID_SHORT()",
        "Category" => $category,
        "Link" => $link,
        "Title" => $title,
        "#Date_Added" => "CURRENT_TIMESTAMP()",
        "Author" => $authors,
        "Image_Link" => $imageLink,
        "Summary" => $summary,
        "#Pub_Date" => "STR_TO_DATE('$pubDate', '%m/%d/%Y %l:%i %p')",
        "Source" => $source,
        "All_Summaries" => $all_summaries,
        "Original_Text" => $ot
    ));
}

// Check for any last minute errors
if ($database->error()[1]){
    echo json_encode($database->error()) . "\n";
    return;
}

echo 0; // success

?>