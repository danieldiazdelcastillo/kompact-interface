
parsed_data = [[0,11,"SpaceX's billionaire founder and chief executive, Elon Musk, has said that recovering and reusing rockets could speed up launches and drive down costs."],[1,16,"SpaceX is one of two companies making cargo runs to the ISS for NASA under contracts worth a combined $3.5 billion."],[2,3,"The soonest SpaceX can try again is Friday morning, provided it can fix the problem by then."],[3,4,"SpaceX officials said one of two motors needed for rocket thrust steering of the second stage failed to operate as expected."],[4,7,"That's the primary objective for SpaceX."],[5,2,"But the countdown was halted with just over a minute remaining after engineers spotted a problem with a piece of equipment in the rocket's upper-stage engine."],[6,1,"The unmanned Falcon rocket, carrying a Dragon cargo ship for the ISS, was due to lift off from Cape Canaveral Air Force Station before sunrise."],[7,6,"The Dragon capsule aboard the rocket contains more than 5,000 pounds of supplies and experiments ordered by NASA."],[8,0,"U.S. firm SpaceX canceled its planned flight to the International Space Station ( ISS ) at the last minute on Tuesday because of technical trouble."],[9,15,"The test was repeated successfully, paving the way for Tuesday's attempt."],[10,8,"But the California-based company plans to attempt an even more extraordinary feat once the Dragon is on its way: landing the booster rocket on a floating platform off Florida's east coast."],[11,5,"If controllers had not aborted the launch, computers likely would have done so closer to liftoff, officials said."],[12,10,"If successful, the test will mark a significant step in the company's quest to develop rockets that can be refurbished and reflown."],[13,9,"No one has ever pulled off such a touchdown."],[14,12,"\" If you were to throw the airplane away after every trip you take it's going to be expensive, \" SpaceX vice president Hans Koenigsmann, told reporters at a prelaunch press conference."],[15,13,"\" This would have an impact on the entire industry.\""],[16,14,"The delivery was supposed to be before Christmas, but it was delayed because of a flawed test firing of the rocket engines."],[17,17,"NASA's last contracted shipment was destroyed in October in an explosion seconds after liftoff."],[18,18,"The company running that mission, Orbital Sciences Corp., has grounded its rocket fleet until later this year."]];

var parsed_html = [];
var all_summaries = [];
var min_char_count = 100;
var words_per_minute = 250/60;

for (var i = 0; i <= parsed_data.length; i++){
   all_summaries[i] = filter(i);
}

for (var i = 0; i < all_summaries.length; i++){
	var container = $("<div></div>");
	for (var j = 0; j < all_summaries[i].length; j++){
		var sentence = all_summaries[i][j];

		if (sentence.indexOf("\"") == 0 && sentence.indexOf("\"",sentence.length-1) != -1){
			var p = $("<p></p>");
			p.append(sentence + " ");
			container.append(p);
			continue;
		}

		if (container.children().last().text().length >= min_char_count || container.children().length == 0){
			container.append($("<p></p>"));
		}

		container.children().last().append(sentence + " ");
	}
	parsed_html.push(container.html());
}

//$("#summary-text").html(parsed_html[parsed_html.length-1]);
summarize(100);

// bootstrap slider
$('#slider').slider({
	// this function is called on load
	formatter: function(value){
		summarize(value);
		return value;
	}
});

function summarize(value){
	$(".summary-text").html(parsed_html[parseInt((parsed_html.length-1)*parseInt(value)/100.0)]);
	$("#percent-reading-time").text(value + "% / " + percent_to_time(parseInt(value)));
	$("#summary-button").text("Summarize to (" + value + "%)");
};

// Compare the original order of the sentences in the parsed data.
function Comparator(a,b){
    if (a[1] < b[1]) return -1;
    if (a[1] > b[1]) return 1;
    return 0;
};

// The filter function.
function filter(retain){
    var sentences = [];
    var sub = parsed_data.slice(0,retain);
    sub = sub.sort(Comparator);
    sub.forEach(function(sentence){
        sentences.push(sentence[2].replace(/u00a0/g," ").replace(/u00c2 #/g,"£"));
    });
    return sentences;
};

function percent_to_time(percent){
	var summary = all_summaries[parseInt((all_summaries.length-1)*percent/100.0)].join(" ");
	var word_count = summary.split(" ").length; // get the approximate number of words
	var reading_time = Math.ceil(word_count/words_per_minute); // approximate reading time in seconds

	// reformat the reading time into a string
	var minutes = Math.floor(reading_time/60);
	var seconds = reading_time % 60;
	var expression = minutes + "m " + seconds + "s";
	return expression;
};