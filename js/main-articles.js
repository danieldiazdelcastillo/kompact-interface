var $container 	= $( '.flip' ),
	$pages		= $container.children().hide();
var panelIsOpen = false;
var previousContainerHTML = $(".flip").html();

Modernizr.load({
	test: Modernizr.csstransforms3d && Modernizr.csstransitions,
	yep : ['js/jquery.tmpl.min.js','js/jquery.history.js','js/core.string.js','js/jquery.touchSwipe-1.2.5.js','js/jquery.flips.js'],
	nope: 'css/fallback.css',
	callback : function( url, result, key ) {
		
		if( url === 'css/fallback.css' ) {
			$pages.show();
		}
		else if( url === 'js/jquery.flips.js' ) {
			addPage("5-articles-template.html");
		}

	},
	complete: function(){ // called after loading all scripts


		// Add arrow key functionality
		$(document).keydown(function(e) {
		    if (e.which === 37 && !panelIsOpen){ // left
		    	turnToPrevPage();
		    }
		    else if (e.which === 39 && !panelIsOpen){ // right
		    	turnToNextPage();
		    }
		    else {
		    	return; // exit this handler for other keys
		    }
		    e.preventDefault(); // prevent the default action (scroll / move caret)
		});

		// Open left side panel
		$("#menu-sandwich").panelslider({
			side: 'left',      // Panel side: left or right
			duration: 200,     // Transition duration in miliseconds
			clickClose: true,  // If true closes panel when clicking outside it
			onOpen: function(){ // When supplied, function is called after the panel opens
				panelIsOpen = true;
			},
			onClose: function(){
				panelIsOpen = false;
			}
		});

	    addPage("4-articles-template.html");
	    addPage("5-articles-template-2.html");
	}
});

function turnToNextPage(){
	// Flip towards left to next page
	var startingX = Math.ceil($(window).width()/2) + 1; // start at a point on the right side of the screen
	var endingX = startingX-1; // move to the left of the previous point
	var startingY = 1;

	touchStartFunc(startingX, startingY);
	touchMoveFunc(endingX, startingY);
	touchEndFunc();
};

function turnToPrevPage(){
	// Flip towards right to prev page
	var startingX = Math.ceil($(window).width()/2) - 1; // start at a point on the left side of the screen
	var endingX = startingX+1; // move to the right of the previous point
	var startingY = 1;

	touchStartFunc(startingX, startingY);
	touchMoveFunc(endingX, startingY);
	touchEndFunc();
};

function resetPages(){
	// Change cover photo
	$(".page.cover .front .content, .first-story-body").css("background-image", "url('http://america.aljazeera.com/content/ajam/articles/2015/1/6/spacex-cancels-launch/jcr:content/headlineImage.adapt.1460.high.1420563318114.jpg')");

	$(".next").click(function(){
		turnToNextPage();
	});

	$(".prev").click(function(){
		turnToPrevPage();
	});

	// Set date
	var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
	var now = new Date();
	$(".date").text(months[now.getMonth()] + " " + now.getDate() + ", " + now.getFullYear());

	// Remove padding dynamically
    $(".page.cover .back .inner, .page:eq(1) .front .inner").css({
    	"padding": "0"
    });
};

function getHTML(file){
	var html;
	$.ajax({
		url: file,
		async: false,
		success: function(data){
			html = data;
		}
	});
	return html;
};

function addPage(file){
	var replacement = $("<div class='flip container'></div>");
	replacement.html(previousContainerHTML);
	$(".flip").replaceWith(replacement);

	$(".f-page.f-cover-back").before(getHTML(file));
	previousContainerHTML = $(".flip").html();
	$(".flip").flips();
	resetPages();
};