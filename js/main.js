
var selectedCategory;
var authors = [];
var min_char_count = 100;
var parsed_data;


// Print errors
window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
    alert('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber
    + ' Column: ' + column + ' StackTrace: ' +  errorObj);
}

window.onload = function(){
    loadData();
};

// Smooth scrolling
$(function() {
    $('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});


// Main functions

// Initialize category pickers
$(".category-selection label.btn").click(function(){
    $("#selected-category").text($(this).text());
    selectedCategory = $(this).text();
});

// Initialize datetimepicker
if ($('#datetimepicker1').length)
    $('#datetimepicker1').datetimepicker();

// Initialize author additions
$("#add-author").click(function(){
    authors.push($("#author-to-add").val());
    $("#author-to-add").val("");

    $("#authors").text(authors.join(", "));
});
$("#clear-authors").click(function(){
    authors = [];
    $("#authors").text("(none added)");
});

// Initialize image checkers
$("input.image1, input.image2, input.image3").on("change keyup paste", function(){
    $("img." + $(this).attr("id")).attr("src", $(this).val());
});


// Convert a title that's all caps to only the first letter capitalized
$("#regularize").click(function(){
    var strings = $("#article-title").val().trim().split(/\s+/);
    for (var i = 0; i < strings.length; i++)
        strings[i] = strings[i].charAt(0).toUpperCase() + strings[i].slice(1).toLowerCase();
    $("#article-title").val(strings.join(" "));
});


// Summarize button
$("#summarize").click(function(){

    if ($("#text-to-summarize").val().trim() === ""){
        showAlert("Enter text in the text box before you generate a summary");
        return true;
    }

    $(this).addClass("disabled");
    $("#text-to-summarize").prop("disabled", true);

    var postRequestURL = "http://localhost:8081";

    $.ajax({
        type: "POST",
        url: "../php/request.php",
        timeout: 300000, // 300 seconds
        data: {
            "text": $("#text-to-summarize").val(),
            "url": postRequestURL
        },
        success: function(data, textStatus){
            $("#summary-result").text(data); // JSON encoded data
            $("#all-summaries").text(JSON.stringify(initializeSummary(data))); // all html summaries
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert([jqXHR, textStatus, errorThrown]);
        },
        complete: function(jqXHR, textStatus){
            $("#text-to-summarize").prop("disabled", false);
            $("#summarize").removeClass("disabled");
        }
    });

});

// Summary: $("#summary-result").text()
// Category: selectedCategory
// Link: $("#article-link").val()
// Title: $("#article-title").val()
// Authors (optional):  authors.join(", ")
// Pub date (optional): $("#datetimepicker1 > input[type='text']").val()
// Image links: $("input.image1").val(), $("input.image2").val(), $("input.image3").val()
function isMissingInfo(){

    if ($("#summary-result").text().trim() === ""){
        showAlert("Missing the JSON summary");
        return true;
    }

    else if (typeof selectedCategory === 'undefined'){
        showAlert("Category not selected");
        return true;
    }

    else if ($("#article-link").val().trim() === ""){
        showAlert("Link is missing");
        return true;
    }

    else if ($("#article-title").val().trim() === ""){
        showAlert("Title is missing");
        return true;
    }

    else if ($("input.image1").val().trim() === ""){
        showAlert("Image source is missing");
        return true;
    }

    return false;
};

function showAlert(alertMessage){
    $("#modal .modal-body").html(alertMessage);
    $("#modal").modal();
};

// Preview
$("#preview").click(function(){
    if (isMissingInfo())
        return;
});

// Add to DB
// Summary: $("#summary-result").text()
// Category: selectedCategory
// Link: $("#article-link").val()
// Title: $("#article-title").val()
// Authors (optional):  authors.join(", ")
// Pub date (optional): $("#datetimepicker1 > input[type='text']").val()
// Image links: $("input.image1").val(), $("input.image2").val(), $("input.image3").val()
$("#submit").click(function(){
    if (isMissingInfo())
        return;

    // Disable the button first
    $("#submit").addClass("disabled");

    $.ajax({
        type: "POST",
        url: $("#local-checkbox").is(":checked") ? "../php/push-locally.php" : "../php/request-push-to-database.php",
        timeout: 300000, // 300 seconds
        data: {
            "as": $("#all-summaries").text().trim(),
            "s": $("#summary-result").text().trim(),
            "c": selectedCategory.trim(),
            "l": $("#article-link").val().trim(),
            "t": $("#article-title").val().trim(),
            "a": authors.join(", ").trim(),
            "p": $("#datetimepicker1 > input[type='text']").val().trim(),
            "i": $("input.image1").val().trim(),
            "source": $("#article-source").val().trim(),
            "ot": $("#text-to-summarize").val().trim()
        },
        success: function(data, textStatus){
            if (data == "0")
                alert("success");
            else
                showAlert(data);
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert([jqXHR, textStatus, errorThrown]);
        },
        complete: function(jqXHR, textStatus){
            $("#submit").removeClass("disabled");
        }
    });
});


$("#article-search button.push").click(function(){

    if ($("#article-search input").val().trim() === ""){
        showAlert("Missing article id");
        return;
    }

    if (isMissingInfo())
        return;

    // Disable the button first
    $("#article-search button").addClass("disabled");

    $.ajax({
        type: "POST",
        url: $("#local-checkbox").is(":checked") ? "../php/push-locally.php" : "../php/request-push-to-database.php",
        timeout: 300000, // 300 seconds
        data: {
            "as": $("#all-summaries").text().trim(),
            "s": $("#summary-result").text().trim(),
            "c": selectedCategory.trim(),
            "l": $("#article-link").val().trim(),
            "t": $("#article-title").val().trim(),
            "a": authors.join(", ").trim(),
            "p": $("#datetimepicker1 > input[type='text']").val().trim(),
            "i": $("input.image1").val().trim(),
            "source": $("#article-source").val().trim(),
            "ot": $("#text-to-summarize").val().trim(),
            "id": $("#article-search input").val().trim()
        },
        success: function(data, textStatus){
            if (data == "0")
                alert("successful update");
            else
                showAlert(data);
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert([jqXHR, textStatus, errorThrown]);
        },
        complete: function(jqXHR, textStatus){
            $("#article-search button").removeClass("disabled");
        }
    });
});



$("#save").click(function(){
    saveData();
});

$("#article-search button.search").click(function(){
    if ($("#article-search input").val().trim() === ""){
        showAlert("Fill in search box");
        return;
    }

    $("#article-search button").addClass("disabled");

    $.ajax({
        type: "GET",
        url: ($("#local-checkbox").is(":checked") ? "../php/requestFromLocalServer.php?id=" : "../php/requestFromServer.php?id=") + $("#article-search input").val(),
        success: function(data){
            var articles = JSON.parse(data);

            if (articles.length === 0)
                showAlert("could not find article");
            else{

                var article = articles[0];

                $("#text-to-summarize").val(article["Original_Text"]);
                $("#all-summaries").text(article["All_Summaries"]);
                $("#summary-result").text(article["Summary"]);
                selectedCategory = article["Category"];
                $(".category-selection label.btn").each(function(index){
                    if ($(this).text().trim() === selectedCategory){
                        $(this).trigger("click");
                    }
                });
                $("#article-link").val(article["Link"]);
                $("#article-title").val(article["Title"]);
                authors = article["Author"].split(", ");
                $("#authors").text(authors.join(", "));
                $("#datetimepicker1 > input[type='text']").val(article["Pub_Date"]);
                $("input.image1").val(article["Image_Link"]);
                $("img.image1").attr("src", article["Image_Link"]);
                $("#article-source").val(article["Source"]);

            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert([jqXHR, textStatus, errorThrown]);
        },
        complete: function(){
            $("#article-search button").removeClass("disabled");
            alert("done loading article");
        }
    });
});

function saveData(){
    localStorage.setItem("original-text", $("#text-to-summarize").val());
    localStorage.setItem("all-summaries", $("#all-summaries").text());
    localStorage.setItem("summary-text", $("#text-to-summarize").val());
    localStorage.setItem("summary", $("#summary-result").text());
    if (selectedCategory)
        localStorage.setItem("category", selectedCategory.trim());
    localStorage.setItem("link", $("#article-link").val());
    localStorage.setItem("title", $("#article-title").val());
    localStorage.setItem("authors", JSON.stringify(authors));
    localStorage.setItem("pubDate", $("#datetimepicker1 > input[type='text']").val());
    localStorage.setItem("imgLinks", $("input.image1").val());
    localStorage.setItem("source", $("#article-source").val());

    console.log("saving localStorage: " + JSON.stringify(localStorage));
};

function loadData(){
    //console.log("loading localStorage: " + JSON.stringify(localStorage));

    if (localStorage.getItem("original-text"))
        $("#text-to-summarize").val(localStorage.getItem("original-text"));
    if (localStorage.getItem("all-summaries"))
        $("#all-summaries").text(localStorage.getItem("all-summaries"));
    if (localStorage.getItem("summary-text"))
        $("#text-to-summarize").val(localStorage.getItem("summary-text"));
    if (localStorage.getItem("summary"))
        $("#summary-result").text(localStorage.getItem("summary"));
    if (localStorage.getItem("category")){
        selectedCategory = localStorage.getItem("category");
        $(".category-selection label.btn").each(function(index){
            if ($(this).text().trim() === selectedCategory){
                $(this).trigger("click");
            }
        });
    }
    if (localStorage.getItem("link"))
        $("#article-link").val(localStorage.getItem("link"));
    if (localStorage.getItem("title"))
        $("#article-title").val(localStorage.getItem("title"));
    if (localStorage.getItem("authors")){
        authors = JSON.parse(localStorage.getItem("authors"));
        $("#authors").text(authors.join(", "));
    }
    if (localStorage.getItem("pubDate"))
        $("#datetimepicker1 > input[type='text']").val(localStorage.getItem("pubDate"));
    if (localStorage.getItem("imgLinks")){
        $("input.image1").val(localStorage.getItem("imgLinks"));
        $("img.image1").attr("src", localStorage.getItem("imgLinks"));
    }
    if (localStorage.getItem("source"))
        $("#article-source").val(localStorage.getItem("source"));
};


function initializeSummary(jsonSummary){
    parsed_data = JSON.parse(jsonSummary);

    var all_summaries = [];
    var parsed_html = [];
    for (var i = 0; i <= parsed_data.length; i++){
       all_summaries[i] = filter(i);
    }

    for (var i = 0; i < all_summaries.length; i++){
        var container = $("<div></div>");
        for (var j = 0; j < all_summaries[i].length; j++){
            var sentence = all_summaries[i][j];

            if (sentence.indexOf("\"") == 0 && sentence.indexOf("\"",sentence.length-1) != -1){
                var p = $("<p></p>");
                p.append(sentence + " ");
                container.append(p);
                continue;
            }

            if (container.children().last().text().length >= min_char_count || container.children().length == 0){
                container.append($("<p></p>"));
            }

            container.children().last().append(sentence + " ");
        }
        parsed_html.push(container.html());
    }

    return parsed_html;

};

// The filter function.
function filter(retain){
    var sentences = [];
    var sub = parsed_data.slice(0,retain);
    sub = sub.sort(Comparator);
    sub.forEach(function(sentence){
        sentences.push(sentence[2]);
    });
    return sentences;
};

function Comparator(a,b){
    if (a[1] < b[1]) return -1;
    if (a[1] > b[1]) return 1;
    return 0;
};


