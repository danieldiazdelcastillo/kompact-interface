<!DOCTYPE html>

<?php

	// Display any errors
	error_reporting(0);

	// Check if server is up and running
	$raw_text = "Test";
	$options = array(
	    'http' => array(
	        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
	        'method'  => 'POST',
	        'timeout' => 300, // change accordingly
	        'content' => http_build_query(array("text" => $raw_text))
	    )
	);
	$context  = stream_context_create($options);
	$jsonString = file_get_contents("http://localhost:8081/", false, $context);

	if (!$jsonString)
		echo "<h1>Server is not running</h1>";

?>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Kompact Interface</title>

		<!-- Bootstrap -->
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
		<link rel="stylesheet" href="/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />
		<link rel="stylesheet" type="text/css" href="css/main.css"/>

		<?php
			// Check if on localhost
			$whitelist = array(
			    '127.0.0.1',
			    '::1'
			);

			if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
				echo '<script type="text/javascript">var onLocalhost = false;</script>';
			}
			else {
				echo '<script type="text/javascript">var onLocalhost = true;</script>';
			}
		?>

	</head>
	<body>

		<!-- Alerts -->
		<div id="modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Error</h4>
					</div>
					<div class="modal-body">
						...
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="page-header">
					<h1>Kompact Interface</h1>
				</div>
			</div>

			<div class="row">

				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary">
								<div class="panel-heading">Article text</div>
								<div class="panel-body">

									<p>Copy and paste the article into here. Make sure just the essentail article text is here. No headers/subheaders, embedded links, ads, etc.</p>

									<textarea id="text-to-summarize" class="form-control" style="resize: none; min-height: 500px;" placeholder="Hac fringilla netus consequat hymenaeos tellus malesuada vestibulum. Aliquet enim ..."></textarea>

									<br>
									<p>
										<button id="summarize" class="btn btn-primary">Generate Summary</button>
									</p>

								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary">
								<div class="panel-heading">JSON Summary</div>
								<div class="panel-body">

									<p>The JSON summary will appear below once it is finished generating.</p>
									<hr>
									<p style="max-height: 500px; overflow-y: scroll; padding: 10px; border: 1px solid blue;" id="summary-result"></p>

								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary">
								<div class="panel-heading">All Summaries</div>
								<div class="panel-body">

									<p>All possible summaries will appear below once they are finished generating.</p>
									<hr>
									<p style="max-height: 500px; overflow-y: scroll; padding: 10px; border: 1px solid blue;" id="all-summaries"></p>

								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-6">

					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary">
								<div class="panel-heading">Category</div>
								<div class="panel-body">

									<p>Select a category: <b><span id="selected-category">(none selected)</span></b></p>

									<div class="btn-group category-selection" data-toggle="buttons">
										<label class="btn btn-default">
											<input type="radio" name="options" autocomplete="off"> Business
										</label>
										<label class="btn btn-default">
											<input type="radio" name="options" autocomplete="off"> Politics
										</label>
										<label class="btn btn-default">
											<input type="radio" name="options" autocomplete="off"> US
										</label>
										<label class="btn btn-default">
											<input type="radio" name="options" autocomplete="off"> World
										</label>
										<label class="btn btn-default">
											<input type="radio" name="options" autocomplete="off"> Tech
										</label>
										<label class="btn btn-default">
											<input type="radio" name="options" autocomplete="off"> Science
										</label>
										<label class="btn btn-default">
											<input type="radio" name="options" autocomplete="off"> Health
										</label>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary">
								<div class="panel-heading">Link of original article</div>
								<div class="panel-body">
									<input id="article-link" type="text" class="form-control" placeholder="http://newspaper.com/article" />
								</div>
							</div>
							<div class="panel panel-primary">
								<div class="panel-heading">Title of original article</div>
								<div class="panel-body">
									<input id="article-title" type="text" class="form-control" placeholder="Temparatures plummeting across the US" />
									<div class="btn-group" role="group" aria-label="..." style="margin-top: 10px;">
										<button id="regularize" type="button" class="btn btn-info">ALL UPPERCASE -> All Uppercase</button>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary">
								<div class="panel-heading">Source/Publisher of original article</div>
								<div class="panel-body">
									<input id="article-source" type="text" class="form-control" placeholder="Business Insider" />
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary">
								<div class="panel-heading">Author(s) of original article</div>
								<div class="panel-body">

									<p>Author(s): <b><span id="authors">(none added)</span></b></p>

									<div class="input-group">
										<input id="author-to-add" type="text" class="form-control" placeholder="John Doe">
										<span class="input-group-btn">
											<button id="add-author" class="btn btn-default" type="button">Add author</button>
											<button id="clear-authors" class="btn btn-danger">Clear authors</button>
										</span>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary">
								<div class="panel-heading">Publication date of orginal article</div>
								<div class="panel-body">

					                <div class='input-group date' id='datetimepicker1'>
					                	<input type='text' class="form-control" />
					                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					                    </span>
					                </div>

								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-primary">
								<div class="panel-heading">Links to Images associated with the article</div>
								<div class="panel-body">

									<!--<p><b>At least 1 image is required, though 3 is recommended </b></p>-->

									<p><img class="image1" src="" alt="First image will preview here" /></p>
									<p><input id="image1" type="text" class="form-control image1" placeholder="http://website.com/image1" /></p>

									<!--<p><img class="image2" src="" alt="Second image will preview here" /></p>
									<p><input id="image2" type="text" class="form-control image2" placeholder="http://website.com/image2 (optional)" /></p>

									<p><img class="image3" src="" alt="Third image will preview here" /></p>
									<p><input id="image3" type="text" class="form-control image3" placeholder="http://website.com/image3 (optional)" /></p>-->

								</div>
							</div>
						</div>
					</div>

				</div>

			</div>

			<hr>

			<div class="row">
				<div class="col-md-12 text-center">
					<div class="checkbox">
					  <label>
					    <input id="local-checkbox" type="checkbox" value="">
					    Use local Database
					  </label>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 text-center">
					<button id="save" class="btn btn-default">Save Data Locally on Browser</button>
					<button id="submit" class="btn btn-primary">Add to Database</button>
				</div>
			</div>
			<div class="row" style="margin-top: 10px;">
				<div class="col-md-6 col-md-offset-3">
					<div id="article-search" class="input-group">
						<input type="text" class="form-control" placeholder="Find article with Article_ID">
						<span class="input-group-btn">
							<button class="btn btn-default search" type="button"><span class="glyphicon glyphicon-search"></span></button>
							<button class="btn btn-primary push" type="button">Update article</button>
						</span>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<p style="visibility: hidden; height: 100px;">1</p>
				</div>
			</div>

		</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/jquery.min.js"></script>
		<script type="text/javascript" src="/bower_components/moment/min/moment.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript" src="js/main.js"></script>
	</body>
</html>